
## Que la Forge soit avec Nous !

![fond](res/Brigit%20et%20Komit%20avec%20fond%20forge%20et%20logo%20large%20L.png)<!-- .element height="80%" width="80%" -->

<aside class="notes">
    Question : les termes git / githun / gitlab sont-ils étrangés à qqun ?
</aside>

---

# La Forge c'est ...


----

## La Forge c'est ... une volonté institutionnelle 

- portée par la Direction au Numérique Éducatif 
- pilotée par la mission logiciels et ressources éducatives libres (Alexis Kauffman)
- de promotion des ressources éducatives libres

![fond](res/Logo_Ressources_Educatives_Libres_(REL)_mondial.svg)<!-- .element height="30%" width="30%" -->


----

## La Forge c'est ... une volonté institutionnelle

>"les communs numériques constituent l'horizon par défaut des projets soutenus et opérés par notre ministère... Codes, données et contenus que nous développons ont vocation première à être libres, ouverts et interopérables ..."


<small>Audran Le Baron , Directeur du numérique pour l'éducation
</br>clôture de la Journée du Libre Éducatif 29/03/2024</small>


---

## La Forge c'est ... basé sur Gitlab

... et Gitlab est basé sur Git
 
----

![Git vu par xkcd.com](https://imgs.xkcd.com/comics/git.png)

<small>xkcd.com - CC BY-NC</small>

----

### Git c'est ...

- un outil de gestion de version décentralisé
- exigeant (demande de la rigueur)  
- efficace  
- (une histoire intéressante)

----

## Git c'est ...

## une histoire de commits en trois actes

 - un fichier est modifié
 - puis il est indexé 
 - et enfin validé (= commit)

un commit = un instantanné du projet

----

un commit = une information

![commit par xkcd](https://imgs.xkcd.com/comics/git_commit.png)


<small>xkcd.com - CC BY-NC</small>

----

## Git c'est ...

 - en ligne (exemple : La Forge)  
 - en local  
 - les deux  

----

## Git c'est ...

## ... des branches, des fourches, des clones

- branche : développement d'une nouvelle fonctionnalité sans tout casser
- fork : bifurcation d'un projet  
- clone : récupération un projet dans son répertoire perso  

---

### Qu'est-ce qu'on y trouve ?

Des projets mondialement connus :

![capytale](res/forge_capytale.png)<!-- .element height="30%" width="30%" -->

![nuiducode](res/forge_nuitducode.png)<!-- .element height="30%" width="30%" -->

----

### Qu'est-ce qu'on y trouve ?

Des ressources utiles :

![codex](res/forge_codex.png)<!-- .element height="30%" width="30%" --> 

![annales](res/forge_annales-bac.png)<!-- .element height="30%" width="30%" -->

----

### Qu'est-ce qu'on y trouve ?

Des cours :

![cours](res/forge_cours.png)<!-- .element height="80%" width="80%" -->

---


## Travaux Pratiques !
  
- Préalable : avoir activé son compte sur [apps.education.fr](apps.education.fr)  
- se connecter à apps.education.fr  
- Les Services -> Forge des communs numériques éducatifs  

----

## TP1 : se présenter

- Objectifs :
  - découvrir la gestion des tickets
  - découvrir l'IDE
  - modifier une page


----

## TP1 : se présenter

- sur la Forge :
    - groupe : info IREM Aix-Marseille
    - projet : Atelier Forge 


----

## TP2 : cloner un projet

Utiliser un des modèles proposés pour créer un site incluant
des parties de codes (python / sql) exécutables.

[https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/)

---

## Conclusion

- doublons nuage / capytale ?  
- autoformation  
- la voie est libre ...  

---

## Quelques sites utiles

- [documentation de la forge](https://docs.forge.apps.education.fr/)
- [aeif.fr](aeif.fr)
- [salon de discussion sur Tchap](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.intradef.tchap.gouv.fr)


----

# MERCI !