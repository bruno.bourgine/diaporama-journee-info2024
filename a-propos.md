Diaporama réalisé par Bruno Bourgine pour l'[IRES d'Aix-Marseille](https://sciences.univ-amu.fr/fr/departements/ires).

Hébergé sur la [Forge des Communs Numérique Éducatifs](https://forge.appseducation.fr).

<!--

Choisissez ci-dessous la licence de votre diaporama
en ajoutant un x entre les crochets de votre choix :

[ ] Tout droit réservé
[ ] CC BY NC ND
[ ] CC BY ND
[ ] CC BY NC SA
[ ] CC BY SA	
[x] CC BY NC	
[ ] CC BY	
[ ] CC ZERO	

Pour savoir quelle licence choisir, consultez ce tableau :
https://link.infini.fr/licencescc

La licence choisie sera automatiquement affichée dans
l'onglet "A propos" du menu de votre diaporama.

-->
